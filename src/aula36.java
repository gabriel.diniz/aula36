import java.util.Scanner;

public class aula36 { // operador ternário

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Qual o seu dia da semana predileto?");  // imprimi mensagem
        System.out.println("Digite 1 para segunda e 7 para domingo: "); // imprimi mensagem
        int num = in.nextInt();  // variavel num

        //utilizando operação ternaria
        /*String dia = (num==1) ? "Segunda" :  // SE segunda SENÃO
                        (num==2) ? "Terça" :  // SE terça SENÃO
                        (num==3) ? "Quarta" :  // SE quarta SENÃO
                        (num==4) ? "Quinta" :  // SE quinta SENÃO
                        (num==5) ? "Sexta" :  // SE sexta SENÃO
                        (num==6) ? "Sábado" :  // SE sábado SENÃO
                        (num==7) ? "Domingo" :  // SE domingo SENÃO
                                "Dia inválido";  // SENÃO Dia inválido
        */

        //utilizando switch
        String dia;  // string dia
        switch (num){ //instrução  switch variavel num
            case 1 : dia = "segunda";  // variavel
                break;  //se verdadeiro encerra
            case 2 : dia = "terça";
                break; //se verdadeiro encerra
            case 3 : dia = "quarta";
                break; //se verdadeiro encerra
            case 4 : dia = "quinta";
                break; //se verdadeiro encerra
            case 5 : dia = "sexta";
                break; //se verdadeiro encerra
            case 6 : dia = "sábado";
                break; //se verdadeiro encerra
            case 7 : dia = "domingo";
                break; //se verdadeiro encerra
            default: dia = "Dia inválido"; // se diferente de todos
        }

        System.out.println("O dia predileto escolhido é: "+dia); // imprimi mensagem + dia

    }
}
